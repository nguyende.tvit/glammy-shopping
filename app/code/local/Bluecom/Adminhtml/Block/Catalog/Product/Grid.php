<?php
/**
 * @author <de.nguyen@bluecomgroup.com>
 * @date 16/07/2015
 */
class Bluecom_Adminhtml_Block_Catalog_Product_Grid extends Mage_Adminhtml_Block_Catalog_Product_Grid
{
    public function setCollection($collection)
    {

        $pageSize = $collection->getPageSize();
        $curPage = $collection->getCurPage();

        $collection
            ->addAttributeToSelect(array('small_image', 'is_featured'))
            ->setCurPage($curPage)
            ->setPageSize($pageSize);

        return parent::setCollection($collection);
    }
    protected function _prepareColumns()
    {
        $this->addColumn('image', array(
            'header' => Mage::helper('catalog')->__('Image'),
            'align' => 'left',
            'index' => 'image',
            'width'     => '70',
            'renderer' => 'Bluecom_Adminhtml_Block_Widget_Grid_Column_Renderer_Image'
        ));
        $this->addColumn('is_featured', array(
            'header' => Mage::helper('catalog')->__('Featured'),
            'align' => 'center',
            'index' => 'is_featured',
            'width'     => '70',
            'renderer' => 'Bluecom_Adminhtml_Block_Widget_Grid_Column_Renderer_Field'
        ));
        return parent::_prepareColumns();
    }
}