<?php

/**
 * @author <de.nguyen@bluecomgroup.com>
 * @date 16/07/2015
 */
class Bluecom_Adminhtml_Block_Widget_Grid_Column_Renderer_Field extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $field = $this->getColumn()->getIndex();
        switch ($field) {
            case 'is_featured':
                return $row->getData($field) ? 'Yes' : 'No';
            default:
                return $row->getData($field);
        }

    }
}