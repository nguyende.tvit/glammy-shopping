<?php

/**
 * @author <de.nguyen@bluecomgroup.com>
 * @date 16/07/2015
 */
class Bluecom_Adminhtml_Block_Widget_Grid_Column_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $url = (string) Mage::helper('catalog/image')->init($row, 'small_image')->resize(60);
        $out = "<img src=" .$url . " width='60px'/>";
        return $out;
    }
}