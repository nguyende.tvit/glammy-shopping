<?php
/**
 * @author <de.nguyen@bluecomgroup.com>
 * @date 16/07/2015
 */
class Bluecom_Catalog_Block_Product_Widget_Featured extends Mage_Catalog_Block_Product_Abstract
    implements Mage_Widget_Block_Interface
{
    const DEFAULT_PRODUCTS_COUNT = 10;

    protected $_productsCount;

    protected function _construct()
    {
        parent::_construct();
        $this->addPriceBlockType('bundle', 'bundle/catalog_product_price', 'bundle/catalog/product/price.phtml');
    }
    protected function _getProductCollection()
    {
        /** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = Mage::getResourceModel('catalog/product_collection');
        //var_dump(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());die;
        $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->addAttributeToFilter('is_featured', array('eq' => true))
            ->setPageSize($this->getProductsCount())
            ->setCurPage(1);
        return $collection;
    }
    protected function _beforeToHtml()
    {
        $this->setProductCollection($this->_getProductCollection());
        return parent::_beforeToHtml();
    }
    public function getProductsCount()
    {
        if (!$this->hasData('products_count')) {
            $this->setData('products_count', self::DEFAULT_PRODUCTS_COUNT);
        }
        return $this->getData('products_count');
    }
}